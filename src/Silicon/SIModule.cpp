#include "SiModule.h"
#include "llvm/IR/Module.h"

SiModule::SiModule(llvm::StringRef name) : scope(this) {
	module = new llvm::Module(name, context);

	// initialize allocators

	constructor = nullptr;
	destructor = nullptr;
}

SiModule::~SiModule() {
	// release allocators

	delete module;
}

llvm::StringRef SiModule::GetName() const {
	return module->getName();
}