#include "Compiler/ModuleBuilder.h"
#include "llvm/IR/Verifier.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "OutputStream.h"

#define DEBUG_RUN

#ifndef DEBUG_RUN
// new includes for binary output
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/FileSystem.h"
#endif

#ifdef DEBUG_RUN
#define TEST_FILE "../example.si"
#else
#define TEST_FILE "../fibonacci.si"
#endif

int main(int argc, char **argv) {
	llvm::InitializeNativeTarget();
	llvm::InitializeNativeTargetAsmPrinter();

	ModuleBuilder module(TEST_FILE);
	auto* result = module.Build();

#ifdef DEBUG_RUN
	{
		std::string errStr;
		llvm::ExecutionEngine *EE = llvm::EngineBuilder(std::unique_ptr<llvm::Module>(result)).setErrorStr(&errStr).create();

		if (!EE) {
			System::out.println("Failed to construct ExecutionEngine: ", errStr);
			return 1;
		}

		System::out.println("Constructed LLVM module:");
		result->dump();

		bool bDebugError;
		if (llvm::verifyModule(*result, &llvm::errs(), &bDebugError)) {
			System::out.println("Error constructing module");
			return 1;
		}

		System::out.println("Running module with JIT");
		auto* FibF = result->getFunction("main");
		std::vector<llvm::GenericValue> Args;
		EE->runFunction(FibF, Args);
	}
#else
	{   // run the module using JIT
		std::string errStr;
		llvm::ExecutionEngine *EE = llvm::EngineBuilder(std::unique_ptr<llvm::Module>(result)).setErrorStr(&errStr).create();

		if (!EE) {
			System::out.println("Failed to construct ExecutionEngine: ", errStr);
			return 1;
		}

		System::out.println("Constructed LLVM module:");
		result->dump();

		bool bDebugError;
		if (llvm::verifyModule(*result, &llvm::errs(), &bDebugError)) {
			System::out.println("Error constructing module");
			return 1;
		}

		System::out.println("Running module with JIT");
		auto* FibF = result->getFunction("main");
		if (FibF != nullptr) {
			std::vector<llvm::GenericValue> Args;
			EE->runFunction(FibF, Args);
		}
	}

	{	// write the file to a binary file
		auto TargetTriple = llvm::sys::getDefaultTargetTriple();

		std::string Error;
		auto Target = llvm::TargetRegistry::lookupTarget(TargetTriple, Error);

		// Print an error and exit if we couldn't find the requested target.
		// This generally occurs if we've forgotten to initialise the
		// TargetRegistry or we have a bogus target triple.
		if (!Target) {
			llvm::errs() << Error;
			return 1;
		}

		auto CPU = "generic";
		auto Features = "";

		llvm::TargetOptions opt;
		auto RM = llvm::Optional<llvm::Reloc::Model>();
		auto TargetMachine = Target->createTargetMachine(TargetTriple, CPU, Features, opt, RM);

		result->setDataLayout(TargetMachine->createDataLayout());
		result->setTargetTriple(TargetTriple);

		auto Filename = "output.o";
		std::error_code EC;
		llvm::raw_fd_ostream dest(Filename, EC, llvm::sys::fs::F_None);

		if (EC) {
			llvm::errs() << "Could not open file: " << EC.message();
			return 1;
		}

		llvm::legacy::PassManager pass;
		auto FileType = llvm::TargetMachine::CGFT_ObjectFile;

		if (TargetMachine->addPassesToEmitFile(pass, dest, FileType)) {
			llvm::errs() << "TargetMachine can't emit a file of this type";
			return 1;
		}

		pass.run(*result);
		dest.flush();
	}
#endif

	return 0;
}