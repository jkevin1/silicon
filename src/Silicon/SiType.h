#pragma once

#include <stdint.h>
#include "llvm/ADT/ArrayRef.h"

namespace llvm { class Type; }
class BaseContext;

enum class SiTypeID : uint8_t
{
	// signed integer types
	Int8,     // signed 8-bit integer
	Int16,    // signed 16-bit integer
	Int32,    // signed 32-bit integer
	Int64,    // signed 64-bit integer

	// unsigned integer types
	UInt8,    // unsigned 8-bit integer
	UInt16,   // unsigned 16-bit integer
	UInt32,   // unsigned 32-bit integer
	UInt64,   // unsigned 64-bit integer

	// floating point types
	Float32,  // 32-bit floating point value
	Float64,  // 64-bit floating point value

	// miscellaneous types
	Bool,     // boolean value, can be 1-8 bits

	// complex types
	Function, // function pointer
	Array,    // array of an arbitrary type
	String,   // array of characters

	Struct,   // POD data structure
	Class,    // OOP structure with polymorphism
	Union,    // POD data structure that shares memory

	NUMERICAL_TYPES_BEGIN = Int8,
	NUMERICAL_TYPES_COUNT = Float64 - Int8 + 1,
};

static inline const char* SiTypeIDToString(SiTypeID eType)
{
	switch (eType)
	{
	case SiTypeID::Int8:    return "i8";
	case SiTypeID::Int16:   return "i16";
	case SiTypeID::Int32:   return "i32";
	case SiTypeID::Int64:   return "i64";
	case SiTypeID::UInt8:   return "u8";
	case SiTypeID::UInt16:  return "u16";
	case SiTypeID::UInt32:  return "u32";
	case SiTypeID::UInt64:  return "u64";
	case SiTypeID::Float32: return "f32";
	case SiTypeID::Float64: return "f64";
	case SiTypeID::Bool:    return "bool";
	}

	// TODO implement other types
	assert(false);
	return nullptr;
}

static inline bool IsSignedIntegerType(SiTypeID eType)
{
	return (eType == SiTypeID::Int8) || (eType == SiTypeID::Int16) || (eType == SiTypeID::Int32) || (eType == SiTypeID::Int64);
}

static inline bool IsUnsignedIntegerType(SiTypeID eType)
{
	return (eType == SiTypeID::UInt8) || (eType == SiTypeID::UInt16) || (eType == SiTypeID::UInt32) || (eType == SiTypeID::UInt64);
}

static inline bool IsFloatingPointType(SiTypeID eType)
{
	return (eType == SiTypeID::Float32) || (eType == SiTypeID::Float64);
}

static inline bool IsIntegralType(SiTypeID eType)
{
	return IsSignedIntegerType(eType) || IsUnsignedIntegerType(eType);
}

static inline bool IsNumericType(SiTypeID eType)
{
	return IsIntegralType(eType) || IsFloatingPointType(eType);
}

static inline bool GetCompatibleNumericalTypes(SiTypeID* pTypeLHS, SiTypeID* pTypeRHS)
{

	constexpr int nTypes = (int)SiTypeID::NUMERICAL_TYPES_COUNT;
	constexpr SiTypeID NO_LOSSLESS_TYPE = SiTypeID::NUMERICAL_TYPES_COUNT;
	static const SiTypeID TypePromotionMap[nTypes][nTypes] = {
		            /* i8 */           /* i16 */          /* i32 */          /* i64 */         /* u8 */           /* u16 */          /* u32 */          /* u64 */         /* f32 */          /* f64 */
		/* i8  */ { SiTypeID::Int8,    SiTypeID::Int16,   SiTypeID::Int32,   SiTypeID::Int64,  SiTypeID::Int16,   SiTypeID::Int32,   SiTypeID::Int64,   NO_LOSSLESS_TYPE, SiTypeID::Float32, SiTypeID::Float64 },
		/* i16 */ { SiTypeID::Int16,   SiTypeID::Int16,   SiTypeID::Int32,   SiTypeID::Int64,  SiTypeID::Int16,   SiTypeID::Int32,   SiTypeID::Int64,   NO_LOSSLESS_TYPE, SiTypeID::Float32, SiTypeID::Float64 },
		/* i32 */ { SiTypeID::Int32,   SiTypeID::Int32,   SiTypeID::Int32,   SiTypeID::Int64,  SiTypeID::Int32,   SiTypeID::Int32,   SiTypeID::Int64,   NO_LOSSLESS_TYPE, SiTypeID::Float64, SiTypeID::Float64 },
		/* i64 */ { SiTypeID::Int64,   SiTypeID::Int64,   SiTypeID::Int64,   SiTypeID::Int64,  SiTypeID::Int64,   SiTypeID::Int64,   SiTypeID::Int64,   NO_LOSSLESS_TYPE, NO_LOSSLESS_TYPE,  NO_LOSSLESS_TYPE  },
		/* u8  */ { SiTypeID::Int16,   SiTypeID::Int16,   SiTypeID::Int32,   SiTypeID::Int64,  SiTypeID::UInt8,   SiTypeID::UInt16,  SiTypeID::UInt32,  SiTypeID::UInt64, SiTypeID::Float32, SiTypeID::Float64 },
		/* u16 */ { SiTypeID::Int32,   SiTypeID::Int32,   SiTypeID::Int32,   SiTypeID::Int64,  SiTypeID::UInt16,  SiTypeID::UInt16,  SiTypeID::UInt32,  SiTypeID::UInt64, SiTypeID::Float32, SiTypeID::Float64 },
		/* u32 */ { SiTypeID::Int64,   SiTypeID::Int64,   SiTypeID::Int64,   SiTypeID::Int64,  SiTypeID::UInt32,  SiTypeID::UInt32,  SiTypeID::UInt32,  SiTypeID::UInt64, SiTypeID::Float64, SiTypeID::Float64 },
		/* u64 */ { NO_LOSSLESS_TYPE,  NO_LOSSLESS_TYPE,  NO_LOSSLESS_TYPE,  NO_LOSSLESS_TYPE, SiTypeID::UInt64,  SiTypeID::UInt64,  SiTypeID::UInt64,  SiTypeID::UInt64, NO_LOSSLESS_TYPE,  NO_LOSSLESS_TYPE  },
		/* f32 */ { SiTypeID::Float32, SiTypeID::Float32, SiTypeID::Float64, NO_LOSSLESS_TYPE, SiTypeID::Float32, SiTypeID::Float32, SiTypeID::Float64, NO_LOSSLESS_TYPE, SiTypeID::Float32, SiTypeID::Float64 },
		/* f64 */ { SiTypeID::Float64, SiTypeID::Float64, SiTypeID::Float64, NO_LOSSLESS_TYPE, SiTypeID::Float64, SiTypeID::Float64, SiTypeID::Float64, NO_LOSSLESS_TYPE, SiTypeID::Float64, SiTypeID::Float64 },
	};

	if (*pTypeLHS == *pTypeRHS)
	{
		// The types are equal, so they are already compatible
		return true;
	}

	const uint32_t nTypeLHS = static_cast<uint32_t>(*pTypeLHS);
	const uint32_t nTypeRHS = static_cast<uint32_t>(*pTypeRHS);
	const SiTypeID ePromotedType = TypePromotionMap[nTypeLHS][nTypeRHS];

	if (ePromotedType == NO_LOSSLESS_TYPE)
	{
		// Log a warning? Compiler error?
		return false;
	}

	*pTypeLHS = ePromotedType;
	*pTypeRHS = ePromotedType;
	return true;
}

class SiType {
public:
	SiType(SiTypeID eType) : m_eType(eType), m_pTypeDefinition(nullptr) { }
	virtual void ResolveLLVMType(BaseContext& context);
	llvm::Type* GetLLVMType() const { return m_pTypeDefinition; }
	SiTypeID GetTypeID() const { return m_eType; }
private:
	llvm::Type* m_pTypeDefinition;
	SiTypeID m_eType;
};

class SiStruct : public SiType
{
	
};