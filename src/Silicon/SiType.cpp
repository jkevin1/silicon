#include "SiType.h"
#include "llvm/IR/Instructions.h"
#include "Compiler/Context.h"

void SiType::ResolveLLVMType(BaseContext& context)
{
	switch (m_eType)
	{
	case SiTypeID::Int8:    m_pTypeDefinition = llvm::Type::getInt8Ty(context.context);   break;
	case SiTypeID::Int16:   m_pTypeDefinition = llvm::Type::getInt16Ty(context.context);  break;
	case SiTypeID::Int32:   m_pTypeDefinition = llvm::Type::getInt32Ty(context.context);  break;
	case SiTypeID::Int64:   m_pTypeDefinition = llvm::Type::getInt64Ty(context.context);  break;
	case SiTypeID::UInt8:   m_pTypeDefinition = llvm::Type::getInt8Ty(context.context);   break;
	case SiTypeID::UInt16:  m_pTypeDefinition = llvm::Type::getInt16Ty(context.context);  break;
	case SiTypeID::UInt32:  m_pTypeDefinition = llvm::Type::getInt32Ty(context.context);  break;
	case SiTypeID::UInt64:  m_pTypeDefinition = llvm::Type::getInt64Ty(context.context);  break;
	case SiTypeID::Float32: m_pTypeDefinition = llvm::Type::getFloatTy(context.context);  break;
	case SiTypeID::Float64: m_pTypeDefinition = llvm::Type::getDoubleTy(context.context); break;
	case SiTypeID::Bool:    m_pTypeDefinition = llvm::Type::getInt1Ty(context.context);   break;
	
	// The following are complex types that should override this function
//	case TypeID::Function:
//	case TypeID::Array:
//	case TypeID::String:
//	case TypeID::Struct:
//	case TypeID::Class:
//	case TypeID::Union:
	}

	assert(m_pTypeDefinition != nullptr);
}