#pragma once

#include "SiScope.h"
#include "llvm/IR/LLVMContext.h"

class SiFunction;

class SiModule {
public:
	SiModule(llvm::StringRef name);
	~SiModule();

	llvm::StringRef GetName() const;

	SiScope* GetGlobalScope() { return &scope; }
	const SiScope* GetGlobalScope() const { return &scope; }

	void SetModuleConstructor(SiFunction* function) { constructor = function; }
	SiFunction* GetModuleConstructor() const { return constructor; }
	bool HasModuleConstructor() const { return (constructor != nullptr); }

	void SetModuleDestructor(SiFunction* function) { destructor = function; }
	SiFunction* GetModuleDestructor() const { return destructor; }
	bool HasModuleDestructor() const { return (destructor != nullptr); }

private:
	llvm::LLVMContext context;
	llvm::Module* module;

	SiScope scope;

	// AST/String allocators

	SiFunction* constructor;
	SiFunction* destructor;
};