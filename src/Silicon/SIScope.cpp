#include "SiScope.h"

SiScope::SiScope(SiModule* module) {
	this->module = module;
	parent = nullptr;
}

SiScope::SiScope(const SiScope* parent) {
	this->parent = parent;
	module = parent->module;
}

SiScope::~SiScope() {
	// TODO lifetime
}

// TODO:
// GetX() functions to return iterator_range<X> for compiling?
// function overloading... could be a StringMap of lists?
// inline allocation of a map entries then block allocate
// StringMap is supposedly pretty fast, try alternatives

SiType* SiScope::GetType(llvm::StringRef name) const {
	auto type = types.find(name);
	if (type != types.end()) return type->second;
	if (parent == nullptr) return nullptr;
	return parent->GetType(name);
}

SiFunction* SiScope::GetFunction(llvm::StringRef name) const {
	auto function = functions.find(name);
	if (function != functions.end()) return function->second;
	if (parent == nullptr) return nullptr;
	return parent->GetFunction(name);
}

SiVariable* SiScope::GetVariable(llvm::StringRef name) const {
	auto variable = variables.find(name);
	if (variable != variables.end()) return variable->second;
	if (parent == nullptr) return nullptr;
	return parent->GetVariable(name);
}

void SiScope::CreateType(llvm::StringRef name, SiType* type) {
	if (types.find(name) != types.end()) return; // multiple definition
	types.insert({ name, type });
}

void SiScope::CreateFunction(llvm::StringRef name, SiFunction* function) {
	if (functions.find(name) != functions.end()) return; // multiple definition
	functions.insert({ name, function });
}

void SiScope::CreateVariable(llvm::StringRef name, SiVariable* variable) {
	if (variables.find(name) != variables.end()) return; // multiple definition
	variables.insert({ name, variable });

}