#pragma once

#include "llvm/ADT/StringMap.h"

class SiType;
class SiFunction;
class SiVariable;
class SiModule;

class SiScope {
public:
	SiScope(SiModule* module); // construct a root module scope
	SiScope(const SiScope* parent); // construct a child scope
	~SiScope();

	SiType* GetType(llvm::StringRef name) const;
	SiFunction* GetFunction(llvm::StringRef name) const;
	SiVariable* GetVariable(llvm::StringRef name) const;

	void CreateType(llvm::StringRef name, SiType* type);
	void CreateFunction(llvm::StringRef name, SiFunction* function);
	void CreateVariable(llvm::StringRef name, SiVariable* variable);

private:
	const SiScope* parent;
	SiModule* module;

	// TODO inline allocator for these maps
	llvm::StringMap<SiType*     /*Alloc*/> types;
	llvm::StringMap<SiFunction* /*Alloc*/> functions; // TODO overloading?
	llvm::StringMap<SiVariable* /*Alloc*/> variables;
};