#pragma once

#include <stdint.h>

namespace llvm
{
	class Type;
	class Value;
	class Function;
}

// TODO real identifier struct
using Identifier = const char*;

// TODO real type struct
using Type = const char*;

// TODO real variable struct
using Variable = llvm::Value*;

// TODO real function signature struct
using FunctionSignature = const char*;

// TODO real function struct
using Function = const char*;

enum class PrimType : uint8_t
{
	// signed integer types
	Int8,     // signed 8-bit integer
	Int16,    // signed 16-bit integer
	Int32,    // signed 32-bit integer
	Int64,    // signed 64-bit integer

	// unsigned integer types
	UInt8,    // unsigned 8-bit integer
	UInt16,   // unsigned 16-bit integer
	UInt32,   // unsigned 32-bit integer
	UInt64,   // unsigned 64-bit integer

	// floating point types
	Float32,  // 32-bit floating point value
	Float64,  // 64-bit floating point value

	// miscellaneous types
	Bool,     // boolean value, can be 1-8 bits
	// array, char, string...?

	NUM_PRIMITIVES
};

enum class UnaryOp : uint8_t
{
	Negation,             // '-' negation
	BooleanNot,           // '!' boolean negation (TODO should it spelled out 'not')
	BitwiseNot,           // '~' bitwise not
	Invocation,           // '(..)' invocation, such as a function call
	Index,            // '[]' such as indexing into an array

	// TODO cast operations?

	NUM_UNARY_OPS
};


enum class BinaryOp : uint8_t
{
	// arithmetic operations
	Addition,             // '+'   addition
	Subtraction,          // '-'   subtraction
	Multiplication,       // '*'   multiplication
	Division,             // '/'   division
	Modulus,              // '%'   remainder
	
	// boolean operations
	BooleanAnd,           // 'and' logical and
	BooleanOr,            // 'or'  logical or
	// nor, xor...?
	
	// bitwise operations
	ShiftLeft,            // '<<'  binary left shift
	ShiftRight,           // '>>'  binary right shift (logical)
//	ArithmeticShiftRight, // ?
	BitwiseAnd,           // '&'   bitwise and
	BitwiseOr,            // '|'   bitwise or
	BitwiseXOr,           // '^'   bitwise exclusive or

	// comparison operations
	Equal,                // '=='  equal
	NotEqual,             // '!='  not equal
	LessThan,             // '<'   less than
	LessThanOrEqual,      // '<='  less than or equal
	GreaterThan,          // '>'   greater than
	GreaterThanOrEqual,   // '>='  greater than or equal

	// TODO should this be its own Expression?
	Subscript,      // '.'   refer to a member of a type

	// TODO assignment ops +=, -=, etc

	NUM_BINARY_OPS
};