#pragma once

#include <mutex> // ideally this would be in the cpp file
#include <string>

#ifndef _FILE_DEFINED
#define _FILE_DEFINED
struct FILE;
#endif // not sure about other platforms

class OutputStream
{
public:
	// terminal print functions
	void print(const char* s);
	void print(const std::string& str);
	void print(float v);
	void print(char c);

	template<class Arg>
	void print(const Arg& arg)
	{
		fprintf(file, "%p", &arg);
	}

	template<class... Args>
	void print(const Args&... args)
	{
		lock.lock();
		using expand = int[]; // what is this tomfoolery
		expand{ (print(args), 0)... };
		lock.unlock();
	}

	template<class... Args>
	void println(const Args&... args)
	{
		print(args..., '\n');
	}

	// somehow hide this and let extern's use it
	OutputStream(FILE* file) : file(file) { }
	virtual ~OutputStream() { } // hopefully not necessary
private:
	FILE* file;
	std::mutex lock;
};

namespace System {
	extern OutputStream out;
	extern OutputStream err;
}