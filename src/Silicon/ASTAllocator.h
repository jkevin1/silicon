#pragma once

#include <memory>

class ASTAllocator {

public:
	template<class T, class... Args>
	T* Create(Args... args) {
		void* ptr = Alloc(sizeof(T), alignof(T));
		T* object = reinterpret_cast<T*>(ptr);
		new (object) T(args...);
		return object;
	}

	template<class T>
	void Destroy(T* ptr) {
		ptr->~T();
		Free(ptr);
	}

private:
	void* Alloc(uint32_t size, uint32_t alignment);
	void Free(void* ptr);

	struct Block;
	Block* block;
};