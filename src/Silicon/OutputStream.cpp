#include "OutputStream.h"
#include <stdio.h>

void OutputStream::print(const char* arg)
{
	fputs(arg, file);
}

void OutputStream::print(const std::string& str)
{
	fputs(str.c_str(), file);
}

void OutputStream::print(float arg)
{
	// ftoa
	fprintf(file, "%f", arg);
}

void OutputStream::print(char arg)
{
	fputc(arg, file);
}

OutputStream System::out(stdout);
OutputStream System::err(stderr);