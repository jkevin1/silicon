#include "Context.h"
#include "Silicon/SiType.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include <assert.h>

static constexpr uint32_t g_nPrimitives = static_cast<uint32_t>(SiTypeID::NUMERICAL_TYPES_COUNT);
static SiType* g_aPrimitives[g_nPrimitives] = {};
static bool g_bPrimitivesInitialized = false;

//=============================================================================
//=============================================================================
void InitializePrimitiveTypes()
{
	assert(g_bPrimitivesInitialized == false);
	const uint32_t nStart = static_cast<uint32_t>(SiTypeID::NUMERICAL_TYPES_BEGIN);
	const uint32_t nCount = static_cast<uint32_t>(SiTypeID::NUMERICAL_TYPES_COUNT);
	for (uint32_t i = 0; i < nCount; i++)
	{
		const SiTypeID eType = SiTypeID(nStart + i);
		g_aPrimitives[i] = new SiType(eType);
	}
	g_bPrimitivesInitialized = true;
}

//=============================================================================
//=============================================================================
SiType* LookupPrimitiveType(SiTypeID eTypeID)
{
	assert(g_bPrimitivesInitialized == true);
	const uint32_t nIndex = static_cast<uint32_t>(eTypeID);
	assert(nIndex < g_nPrimitives);
	assert(g_aPrimitives[nIndex] != nullptr);
	return g_aPrimitives[nIndex];
}

//=============================================================================
//=============================================================================
BaseContext::BaseContext(llvm::LLVMContext& context, llvm::Module* module) : context(context)
{
	this->parent = nullptr;
	this->module = module;
	this->block = nullptr;

	if (!g_bPrimitivesInitialized)
	{
		InitializePrimitiveTypes();
	}

	// these primitives should probably be checked by the parser so the map lookup can be skipped
	for (uint32_t i = 0; i < g_nPrimitives; i++)
	{
		const SiTypeID eType = SiTypeID(i);
		const char* szTypeName = SiTypeIDToString(eType);
		DefineType(szTypeName, g_aPrimitives[i]);
	}
	DefineType(SiTypeIDToString(SiTypeID::Bool), new SiType(SiTypeID::Bool));

	//DefineType("string", llvm::Type::getInt8PtrTy(context)); // TODO string struct

	llvm::ArrayRef<llvm::Type*> args = { llvm::Type::getInt8PtrTy(context) };
	auto fnType = llvm::FunctionType::get(llvm::Type::getInt32Ty(context), args, true);
	auto func = llvm::Function::Create(fnType, llvm::GlobalValue::ExternalLinkage, "printf", module);
 	DefineVariable("printf", func);
}

BaseContext::BaseContext(const BaseContext& parent, llvm::BasicBlock* block) : context(parent.context)
{
	this->parent = &parent;
	this->module = parent.module;
	this->block = block;
}

BaseContext::~BaseContext()
{
	// empty for now
}

//=============================================================================
//=============================================================================
void BaseContext::DeclareType(Identifier id)
{
	static SiType kErrorType((SiTypeID)0xFF);
	DefineType(id, &kErrorType);
}

void BaseContext::DefineType(Identifier id, SiType* value)
{
//	assert(types.Find(id) == nullptr);	// TODO error log
	types.insert_or_assign(id, value);
}

const SiType* BaseContext::FindType(Identifier id) const
{
	auto type = types.find(id);
	if (type != types.end())
		return type->second;
	if (parent == nullptr)
		return nullptr;
	return parent->FindType(id);
}

//=============================================================================
//=============================================================================
//void BaseContext::DeclareFunction(Identifier id)
//{
//	DefineFunction(id, nullptr);
//}

//void BaseContext::DefineFunction(Identifier id, llvm::Function* value)
//{
//	assert(functions.Find(id) == nullptr);	// TODO error log
//	functions.insert_or_assign(id, value);	// TODO there should be a proxy Variable class
//}

//llvm::Function* BaseContext::FindFunction(Identifier id) const
//{
//	auto function = functions.find(id);
//	if (function != functions.end())
//		return function->second;
//	if (parent == nullptr)
//		return nullptr;
//	return parent->FindFunction(id);
//}

//=============================================================================
//=============================================================================
void BaseContext::DeclareVariable(Identifier id)
{
	DefineVariable(id, nullptr);
}

void BaseContext::DefineVariable(Identifier id, llvm::Value* value)
{
//	assert(variables.Find(id) == nullptr);	// TODO error log
	variables.insert_or_assign(id, value);	// TODO there should be a proxy Variable class
}

llvm::Value* BaseContext::FindVariable(Identifier id) const
{
	auto variable = variables.find(id);
	if (variable != variables.end())
		return variable->second;
	if (parent == nullptr)
		return nullptr;
	return parent->FindVariable(id);
}