#pragma once

#include <stdint.h>
#include <memory>
#include "llvm/ADT/StringRef.h"

class ChunkAllocator
{
public:
	ChunkAllocator();
	~ChunkAllocator();

	void* Alloc(uint32_t nSize, uint32_t nAlign);
	void Free(void* pData);

	const char* AllocString(const char* pString, uint32_t nLength);
	void FreeString(const char* pString);

	inline const char* AllocString(const char* pString)
	{
		uint32_t nLength = strlen(pString);
		return AllocString(pString, nLength);
	}

	template<class T, class... Args>
	inline T* New(Args... args)
	{
		void* pAlloc = Alloc(sizeof(T), alignof(T));
		T* pInstance = reinterpret_cast<T*>(pAlloc);
		new (pInstance) T(args...);
		return pInstance;
	}

	template<class T>
	inline void Delete(T* pInstance)
	{
		pInstance->~T();
		Free(pInstance);
	}

private:
	struct Chunk;
	Chunk* pChunk;
};