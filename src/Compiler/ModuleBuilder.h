#pragma once

#include "AST/ASTBase.h"
#include "AST/Literals.h"
#include "AST/Operations.h"
#include "AST/Variables.h"
#include "AST/Functions.h"
#include "AST/CodeBlock.h"
#include "AST/FlowControl.h"
#include "Silicon/ASTAllocator.h"
#include "Compiler/ChunkAllocator.h"
#include <string>

class Parser;
namespace llvm { class Module; }

//=============================================================================
// This class is intended to construct AST's that can be evaluated and
// turned into Modules and therefore actual executable binaries.  It is
// intended to be passed to a flex/bison parser which will use it to assemble
// an AST from a text file, but can be used directly
//=============================================================================
class ModuleBuilder {
public:
	ModuleBuilder(const char* filename);
	~ModuleBuilder();

	llvm::Module* Build(/* options */);
	void SetModuleCode(StatementList* statements);

	// functions to provide information to the user
	void Log(const char* msg);
	void Warn(const char* warning);
	void Error(const char* error);

	const char* CreateIdentifier(const char* name) {
		return alloc.AllocString(name);
	}

	template<class T, class... Args>
	T* Create(Args... args) {
		return alloc.New<T>(args...);
	}

	Expression* ParseInteger();
	Expression* ParseFloat();
	Expression* ParseString();

private:
	const char* file;
	Parser* parser;
	ChunkAllocator alloc;

	StatementList* code = nullptr;
};