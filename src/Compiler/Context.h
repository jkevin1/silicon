#pragma once

#include "Silicon/SiliconTypes.h"
#include <unordered_map>

namespace llvm {
	class LLVMContext;
	class BasicBlock;
	class Module;
}

class SiType;
enum class SiTypeID : uint8_t;

extern void InitializePrimitiveTypes();
extern SiType* LookupPrimitiveType(SiTypeID eTypeID);

// there are multiple stages for traversing the AST
// stage 1: resolve types
// stage 2: resolve functions, which depend on types
// stage 3: generate IR for each function, which depends on both types and functions

//	potentially these passes could be merged/pipelined/threaded out
//	e.g. function code could be generated that doesn't depend
//	on unresolved types or functions.  Alternatively, functions could have
//	symbolic references to types and the dependencies are resolved later

//=============================================================================
// Stores local variables and functions and can resolve unique ID's
//=============================================================================
class BaseContext
{
public:
	// root context constructor
	BaseContext(llvm::LLVMContext& context, llvm::Module* module);

	// child context constructor
	BaseContext(const BaseContext& parent, llvm::BasicBlock* block);

	BaseContext(const BaseContext&) = delete;

	virtual ~BaseContext();

	void DeclareType(Identifier id);
	void DefineType(Identifier id, SiType* type);
	const SiType* FindType(Identifier id) const;

//	void DeclareFunction(Identifier id);
//	void DefineFunction(Identifier id, llvm::Function* value);
//	llvm::Function* FindFunction(Identifier id) const;

	void DeclareVariable(Identifier id);
	void DefineVariable(Identifier id, llvm::Value* value);
	llvm::Value* FindVariable(Identifier id) const;

	// TODO make these not directly accessible
	const BaseContext* parent;
	llvm::LLVMContext& context;
	llvm::Module* module;
	llvm::BasicBlock* block;

protected:
	struct StrEqual : public std::binary_function<const char*, const char*, bool>
	{
		bool operator()(const char* x, const char* y) const
		{
			return strcmp(x, y) == 0;
		}
	};

	struct StrHash {
		//BKDR hash algorithm
		int operator()(const char* str)const
		{
			int seed = 131;//31  131 1313 13131131313 etc//
			int hash = 0;
			while (*str)
			{
				hash = (hash * seed) + (*str);
				str++;
			}

			return hash & (0x7FFFFFFF);
		}
	};

	std::unordered_map<Identifier, SiType*, StrHash, StrEqual> types;
//	std::unordered_map<Identifier, llvm::Function*, StrHash, StrEqual> functions;
	std::unordered_map<Identifier, llvm::Value*, StrHash, StrEqual> variables;
	/// consider treating functions as variables ;)

};