#include "ChunkAllocator.h"

constexpr size_t ChunkSize = 4096;
constexpr size_t StringAlign = 4;

struct ChunkAllocator::Chunk
{
	uint8_t pData[ChunkSize];
	size_t nRemaining = ChunkSize;
	Chunk* pNextChunk = nullptr;
};

ChunkAllocator::ChunkAllocator()
{
	pChunk = new Chunk();
}

ChunkAllocator::~ChunkAllocator()
{
	while (pChunk != nullptr)
	{
		Chunk* pNextChunk = pChunk->pNextChunk;
		delete pNextChunk;
		pChunk = pNextChunk;
	}
}

void* ChunkAllocator::Alloc(uint32_t nSize, uint32_t nAlign)
{
	// TODO chunks larger than ChunkSize
	if (nSize > pChunk->nRemaining)
	{
		Chunk* pNewChunk = new Chunk();
		pNewChunk->pNextChunk = pChunk;
		pChunk = pNewChunk;
	}

	// TODO alignment is important
	void* pAlloc = pChunk->pData + (ChunkSize - pChunk->nRemaining);
	pChunk->nRemaining -= nSize;
	return pAlloc;
}

void ChunkAllocator::Free(void* pData)
{
	// Nothing
}

const char* ChunkAllocator::AllocString(const char* pString, uint32_t nLength)
{
	void* pAlloc = Alloc(nLength + 1, StringAlign);
	char* pInstance = reinterpret_cast<char*>(pAlloc);
	memcpy(pInstance, pString, nLength + 1);
	return pInstance;
}

void ChunkAllocator::FreeString(const char* pString)
{
	// TODO avoid the const cast
	Free(const_cast<char*>(pString));
}