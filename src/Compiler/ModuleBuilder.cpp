#include "Compiler/ModuleBuilder.h"
#include "Parser/Parser.hpp"
#include "Lexer/Lexer.h"
#include "Compiler/Context.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include <assert.h>
#include <string> // TODO better solution than 'new std::string'
#include <stdio.h>

//=============================================================================
// the Parser class encapsulates all flex and bison code
//=============================================================================
class Parser {
public:
	Parser(const char* filename);
	~Parser();

	void Parse(ModuleBuilder* module);
	int GetLineNum();

	llvm::StringRef GetToken();
private:
	yyscan_t scanner;
	FILE* file;
};

static llvm::LLVMContext Context;

//=============================================================================
// ModuleBuilder implementation
//=============================================================================
ModuleBuilder::ModuleBuilder(const char* filename) {
	parser = new Parser(filename);
	file = strrchr(filename, '/') + 1; // trim just the filename
}

ModuleBuilder::~ModuleBuilder() {
	delete parser;
}

llvm::Module* ModuleBuilder::Build(/* options */) {
	parser->Parse(this);
	
	auto* module = new llvm::Module("test", Context);
	BaseContext context(Context, module);
	CodeBlock block(code);
	block.Generate(context);
	return module;
}

void ModuleBuilder::SetModuleCode(StatementList* statements) {
	code = statements;
}

//=============================================================================
// ModuleBuilder logging functions
//=============================================================================
void ModuleBuilder::Log(const char* msg) {
	int line = parser->GetLineNum();
	printf("[%s:%d] %s\n", file, line, msg);
}

void ModuleBuilder::Warn(const char* warning) {
	int line = parser->GetLineNum();
	printf("[%s:%d] warning: %s\n", file, line, warning);
}

void ModuleBuilder::Error(const char* error) {
	int line = parser->GetLineNum();
	printf("[%s:%d] error: %s\n", file, line, error);
}

//=============================================================================
// ModuleBuilder specialized AST node allocation functions
//=============================================================================
Expression* ModuleBuilder::ParseInteger() {
	auto token = parser->GetToken();
	const int64_t value = atoll(token.data());
	return Create<SignedLiteral>(value);
}

Expression* ModuleBuilder::ParseFloat() {
	auto token = parser->GetToken();
	const double value = atof(token.data());
	return Create<FloatLiteral>(value);
}

Expression* ModuleBuilder::ParseString() {
	auto token = parser->GetToken();
	auto str = new std::string(token.data(), token.size());
	return Create<StringLiteral>(str->c_str());
}

//=============================================================================
// Parser implementation
//=============================================================================
Parser::Parser(const char* filename) {
	file = fopen(filename, "r");
	assert(file != nullptr);

	int result = yylex_init(&scanner);
	assert(result == 0);

	yyset_in(file, scanner);
}

Parser::~Parser() {
	yylex_destroy(scanner);
	fclose(file);
}

void Parser::Parse(ModuleBuilder* module) {
	yyparse(module, scanner);
}

int Parser::GetLineNum() {
	return yyget_lineno(scanner);
}

llvm::StringRef Parser::GetToken() {
	const char* text = yyget_text(scanner);
	int length = yyget_leng(scanner);
	return llvm::StringRef(text, length);
}