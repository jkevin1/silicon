%{
#include "Parser/Parser.hpp"
%}

%option reentrant
%option bison-bridge
%option noline
%option warn
%option nodefault
%option yylineno
%option noyywrap

DIGIT    [0-9]
ID       [a-zA-Z_][a-zA-Z0-9_]*

%%

[ \t\n]                 ;
"and"                   return LOGICAND;
"or"                    return LOGICOR;
"true"                  return BOOL_TRUE;
"false"                 return BOOL_FALSE;
"if"					return IF;
"else"					return ELSE;
"for"					return FOR;
"while"					return WHILE;
"return"                return RETURN;
{ID}                    { yylval->text = yytext; return IDENTIFIER; }
{DIGIT}+"."{DIGIT}*     { yylval->text = yytext; return FLTLITERAL; }
{DIGIT}+                { yylval->text = yytext; return INTLITERAL; }
\"(\\.|[^"])*\"         { yylval->text = yytext; return STRLITERAL; }
"=="				    return EQUAL;
"!="                    return NOTEQUAL;
"<="                    return LESSEQUAL;
">="                    return GREATEREQUAL;
"+="                    return PLUSASSIGN;
"-="                    return MINUSASSIGN;
"*="                    return TIMESASSIGN;
"/="                    return DIVIDEASSIGN;
.                       return *yytext;

%%