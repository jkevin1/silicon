#pragma once

#include "AST/ASTBase.h"
#include "Silicon/SiliconTypes.h"

class VariableDefinition : public Statement {
public:
	VariableDefinition(Identifier type, Identifier name, Expression* initializer);
	virtual void BuildScope(BaseContext& context) { /*TODO*/ }
	virtual void Generate(BaseContext& context) override;
//private:
	Identifier type;
	Identifier name;
	Expression* initializer;
};

class VariableReference : public Expression {
public:
	VariableReference(Identifier name) : name(name) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	Identifier name;
};