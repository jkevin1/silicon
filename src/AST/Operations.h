#pragma once

#include "AST/ASTBase.h"
#include "Silicon/SiliconTypes.h"

class BinaryOperation : public Expression {
public:
	BinaryOperation(Expression* lhs, BinaryOp op, Expression* rhs);
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	Expression* lhs;
	Expression* rhs;
	BinaryOp op;
};

class UnaryOperation : public Expression {
public:
	UnaryOperation(Expression* operand, UnaryOp op);
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	Expression* operand;
	UnaryOp op;
};