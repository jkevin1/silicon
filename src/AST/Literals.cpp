#include "Literals.h"
#include "Silicon/SiType.h"
#include "Compiler/Context.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/GlobalValue.h"
#include <string> // TODO avoid string
#include <assert.h>

// TODO move type mess to context

const SiType* SignedLiteral::GetType(BaseContext& context)
{
	if (value >= INT8_MIN && value <= INT8_MAX)
		return context.FindType("i8");

	if (value >= INT16_MIN && value <= INT16_MAX)
		return context.FindType("i16");

	if (value >= INT32_MIN && value <= INT32_MAX)
		return context.FindType("i32");

	if (value >= INT64_MIN && value <= INT64_MAX)
		return context.FindType("i64");

	return nullptr;
}

llvm::Value* SignedLiteral::Evaluate(BaseContext& context) {
	auto* type = GetType(context);
	assert(type != nullptr);
	return llvm::ConstantInt::getSigned(type->GetLLVMType(), value);
}

const SiType* UnsignedLiteral::GetType(BaseContext& context) {
	
	if (value <= UINT8_MAX)
		return context.FindType("u8");

	if (value <= INT16_MAX)
		return context.FindType("u16");

	if (value <= INT32_MAX)
		return context.FindType("u32");

	if (value <= INT64_MAX)
		return context.FindType("u64");

	return nullptr;
}

llvm::Value* UnsignedLiteral::Evaluate(BaseContext& context) {
	auto* type = GetType(context);
	assert(type != nullptr);
	return llvm::ConstantInt::get(type->GetLLVMType(), value);
}

const SiType* FloatLiteral::GetType(BaseContext& context) {
	llvm::Type* pF32Type = llvm::Type::getFloatTy(context.context);
	llvm::Type* pF64Type = llvm::Type::getDoubleTy(context.context);

	llvm::APFloat v(value);
	if (llvm::ConstantFP::isValueValidForType(pF32Type, v))
		return context.FindType("f32");

	return nullptr;
}

llvm::Value* FloatLiteral::Evaluate(BaseContext& context) {
	auto* type = GetType(context);
	assert(type != nullptr);
	return llvm::ConstantFP::get(type->GetLLVMType(), value);
}

const SiType* BoolLiteral::GetType(BaseContext& context) {
	return context.FindType("bool");
}

llvm::Value* BoolLiteral::Evaluate(BaseContext& context) {
	llvm::Type* pType = llvm::Type::getInt1Ty(context.context);
	return llvm::ConstantInt::get(pType, value ? 1 : 0);
}

static std::string ProcessStringLiteral(llvm::StringRef input) {
	std::string output;
	output.reserve(input.size());
	for (size_t i = 0; i < input.size(); i++) {
		if (input[i] == '\\') {
			switch (input[++i]) {
			default: assert(!"unknown escape character");
			// \a : alert (beep) skipped
			// \b : backspace skipped
			// \f : formfeed skipped
			// \v : vertical tab skipped
			// \? : skipped (no trigraphs)
			// TODO UTF8 support
			case 'n':  output += '\n'; break;
			case 'r':  output += '\r'; break;
			case 't':  output += '\t'; break;
			case '\'': output += '\''; break;
			case '\"': output += '\"'; break;
			case '\\': output += '\\'; break;
			}
		} else {
			output += input[i];
		}
	}
	return std::move(output);
}

const SiType* StringLiteral::GetType(BaseContext& context) {
	llvm::StringRef input(value, length);
	// TODO parse strings at initialization
	std::string string = ProcessStringLiteral(input);
	// TODO support different string types?
	auto* pCharType = llvm::Type::getInt8Ty(context.context);
	return llvm::ArrayType::get(pCharType, string.size());
}

llvm::Value* StringLiteral::Evaluate(BaseContext& context) {
	llvm::StringRef input(value, length);
	std::string string = ProcessStringLiteral(input);
	auto value = llvm::ConstantDataArray::getString(context.context, string); // TODO performance of AddNull=true/false
	auto* global = new llvm::GlobalVariable(*context.module, value->getType(), true, llvm::GlobalValue::InternalLinkage, value);
	auto* zero = llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(context.context));
	llvm::Constant* indices[] = { zero, zero };
	return llvm::ConstantExpr::getGetElementPtr(global->getValueType(), global, indices, true);
}