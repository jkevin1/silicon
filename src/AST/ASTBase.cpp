#include "ASTBase.h"

ExpressionStatement::ExpressionStatement(Expression* expression) {
	this->expression = expression;
}

void ExpressionStatement::Generate(BaseContext& context) {
	expression->Evaluate(context);
}

llvm::Module* CreateModule(StatementList* statements) {
	return nullptr;
}