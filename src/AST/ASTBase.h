#pragma once

namespace llvm { class Type; }
namespace llvm { class Value; }
namespace llvm { class Module; }

class BaseContext;
class SiType;

//=============================================================================
// a statement is a standalone unit of code
//=============================================================================
class Statement {
public:
	virtual ~Statement() { }
	virtual void BuildScope(BaseContext& context) { };
	virtual void Generate(BaseContext& context) = 0;
};

//=============================================================================
// an expression is an element that results in a value
//=============================================================================
class Expression {
public:
	virtual ~Expression() { }
	virtual const SiType* GetType(BaseContext& context) = 0;
	virtual llvm::Value* Evaluate(BaseContext& context) = 0;
};

//=============================================================================
// expressions can be used as statements
//=============================================================================
class ExpressionStatement : public Statement
{
public:
	ExpressionStatement(Expression* expression);
	virtual void BuildScope(BaseContext& context) override { }
	virtual void Generate(BaseContext& context) override;
private:
	Expression* expression;
};

class StatementList;
llvm::Module* CreateModule(StatementList* statements);