#pragma once

#include "AST/ASTBase.h"
#include <vector>

class CodeBlock : public Statement
{
public:
	CodeBlock(StatementList* stmts) : statements(stmts) { }
	virtual void Generate(BaseContext& context);
private:
	StatementList* statements;
};

class StatementList {
public:
	StatementList() { }
	StatementList(Statement* statement) : statements{ statement } { }

	void AddStatement(Statement* statement) { statements.push_back(statement); }
private:
	friend class CodeBlock;
	std::vector<Statement*> statements;
};