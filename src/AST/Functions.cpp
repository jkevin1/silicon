#include "Functions.h"
#include "Variables.h"
#include "../Compiler/Context.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include <string.h> 

#include "llvm/ADT/StringRef.h"
#include "Silicon/SiType.h"

void FunctionDefinition::BuildScope(BaseContext& context)
{
	context.DeclareVariable(name);
}

void FunctionDefinition::Generate(BaseContext& context) {
	// Build argtype list
	std::vector<llvm::Type*> argTypes;
	if (arguments != nullptr) {
		const auto& arguments = this->arguments->arguments;
		argTypes.reserve(arguments.size());
		for (auto& argument : arguments) {
			auto* type = context.FindType(argument->type);
			argTypes.push_back(type->GetLLVMType());
		}
	}

	auto retType = context.FindType(returnTypes[0]); // TODO multiple return types
	auto* ftype = llvm::FunctionType::get(retType->GetLLVMType(), argTypes, false);
	auto* function = (llvm::Function*)context.module->getOrInsertFunction(name, ftype);
//	llvm::Function::Create(ftype, llvm::GlobalValue::InternalLinkage, name, context.module);
	function->setLinkage(llvm::GlobalValue::ExternalLinkage);
	function->setCallingConv(llvm::CallingConv::C);
	context.DefineVariable(name, function);

	auto* block = llvm::BasicBlock::Create(context.context, "entry", function, 0);
	BaseContext scope(context, block);


	int count = 0;
	auto& argList = function->args();
	for (auto& arg : argList)
	{
		scope.DefineVariable(arguments->arguments[count]->name, &arg);
		count++;
	}

	codeBlock->Generate(scope);
//	llvm::ReturnInst::Create(scope.context, scope.block);
}

const SiType* FunctionCall::GetType(BaseContext& context) {
	// TODO evaluate return type
	return nullptr;
}

llvm::Value* FunctionCall::Evaluate(BaseContext& context) {
	// lookup function
	auto func = functor->Evaluate(context);
	
	// evaluate arguments TODO evaluation order
	std::vector<llvm::Value*> args;
	args.reserve(arguments->expressions.size());
	for (auto argument : arguments->expressions)
		args.push_back(argument->Evaluate(context));

	// create instruction4
	auto* call = llvm::CallInst::Create(func, args, "", context.block);
	call->setTailCall(true); // TODO this should just be a hint, not forced to happen
	return call;
}

void ReturnStatement::Generate(BaseContext& context) {
	llvm::Value* result = (value ? value->Evaluate(context) : nullptr);
	llvm::ReturnInst::Create(context.context, result, context.block);
}