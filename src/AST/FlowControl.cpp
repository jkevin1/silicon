#include "FlowControl.h"
#include "Compiler/Context.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"

void Branch::Generate(BaseContext& context) {
	auto* parent = context.block->getParent();

	auto* trueBlock = llvm::BasicBlock::Create(context.context, "true", parent);
	BaseContext trueContext(context, trueBlock);
	statement->Generate(trueContext);

	auto* elseBlock = llvm::BasicBlock::Create(context.context, "else", parent);
	BaseContext elseContext(context, elseBlock);
	elseStatement->Generate(elseContext);

	auto* cond = condition->Evaluate(context);
	llvm::BranchInst::Create(trueBlock, elseBlock, cond, context.block);

	if (trueContext.block->getTerminator() == nullptr || elseContext.block->getTerminator() == nullptr) {
		auto* mergeBlock = llvm::BasicBlock::Create(context.context, "merge", parent);
		if (trueContext.block->getTerminator() == nullptr)
			llvm::BranchInst::Create(mergeBlock, trueContext.block);
		if (elseContext.block->getTerminator() == nullptr)
			llvm::BranchInst::Create(mergeBlock, elseContext.block);
		context.block = mergeBlock;
	}
}

TernaryExpression::TernaryExpression(Expression* condition, Expression* trueVal, Expression* falseVal) {
	cond = condition;
	trueExpr = trueVal;
	falseExpr = falseVal;
}

const SiType* TernaryExpression::GetType(BaseContext& context)
{
	const SiType* pTType = trueExpr->GetType(context);
	const SiType* pFType = falseExpr->GetType(context);
	// TODO ensure valid ternary statement types
	const SiType* pPromotedType = pFType;
	return pPromotedType;
}

llvm::Value* TernaryExpression::Evaluate(BaseContext& context) {
	auto* parent = context.block->getParent();

	auto* trueBlock = llvm::BasicBlock::Create(context.context, "ifTrue", parent);
	BaseContext trueContext(context, trueBlock);
	auto* trueVal = trueExpr->Evaluate(trueContext);

	auto* falseBlock = llvm::BasicBlock::Create(context.context, "ifFalse", parent);
	BaseContext falseContext(context, falseBlock);
	auto* falseVal = falseExpr->Evaluate(falseContext);

	auto* condition = cond->Evaluate(context);
	llvm::BranchInst::Create(trueBlock, falseBlock, condition, context.block);

	// TODO promote to compatible type
//	auto* pTypeA = trueVal->getType();
	auto* pTypeB = falseVal->getType();

	auto* mergeBlock = llvm::BasicBlock::Create(context.context, "merge", parent);
	llvm::BranchInst::Create(mergeBlock, trueContext.block);
	llvm::BranchInst::Create(mergeBlock, falseContext.block);

	auto* phi = llvm::PHINode::Create(pTypeB, 2, "tmp", mergeBlock);
	phi->addIncoming(trueVal, trueContext.block);
	phi->addIncoming(falseVal, falseContext.block);
	context.block = mergeBlock;
	return phi;
}