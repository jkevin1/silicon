#pragma once

#include "AST/ASTBase.h"
#include "AST/Variables.h"
#include "AST/CodeBlock.h"
#include "Silicon/SiliconTypes.h" // TODO better cmake
#include <vector>	// TODO optimized data structures

class ArgumentList {
public:
	ArgumentList() { }
	ArgumentList(VariableDefinition* argument) : arguments{argument} { }

	void AddArgument(VariableDefinition* argument) { arguments.push_back(argument); }
private:
	friend class FunctionDefinition;
	std::vector<VariableDefinition*> arguments;
};

class ExpressionList {
public:
	ExpressionList() { }
	ExpressionList(Expression* expression) : expressions{ expression } { }

	void AddExpression(Expression* expression) { expressions.push_back(expression); }
private:
	friend class FunctionCall;
	std::vector<Expression*> expressions;
};

class FunctionDefinition : public Statement {
public:
	FunctionDefinition(Identifier retType, Identifier name, ArgumentList* args, CodeBlock* code) : name(name), arguments(args), codeBlock(code) { returnTypes.push_back(retType); }
	virtual void BuildScope(BaseContext& context);
	virtual void Generate(BaseContext& context) override;
	// TODO way to add return types/args
	void AddReturnType(Identifier type) { returnTypes.push_back(type); }
private:
	Identifier name;
	ArgumentList* arguments;
	std::vector<Identifier> returnTypes;
	CodeBlock* codeBlock;
};


class FunctionCall : public Expression {
public:
	FunctionCall(Expression* functor, ExpressionList* args) : functor(functor), arguments(args) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	Expression* functor;
	ExpressionList* arguments;
};

class ReturnStatement : public Statement
{
public:
	ReturnStatement(Expression* value = nullptr) : value(value) { }
	virtual void Generate(BaseContext& context) override;
private:
	Expression* value;
};