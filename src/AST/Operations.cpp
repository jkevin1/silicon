#include "Operations.h"
#include "Silicon/SiType.h"
#include "llvm/IR/Instructions.h"
#include "Compiler/Context.h"

BinaryOperation::BinaryOperation(Expression* lhs, BinaryOp op, Expression* rhs) {
	this->lhs = lhs;
	this->op = op;
	this->rhs = rhs;
}



static const SiType* FindCompatibleTypes(BinaryOp eOperator, const SiType* pLHS, const SiType* pRHS)
{
	const SiTypeID eTypeLHS = pLHS->GetTypeID();
	const SiTypeID eTypeRHS = pRHS->GetTypeID();

	if (IsNumericType(eTypeLHS) && IsNumericType(eTypeRHS))
	{
		SiTypeID ePromotedLHS = eTypeLHS;
		SiTypeID ePromotedRHS = eTypeRHS;
		if (GetCompatibleNumericalTypes(&ePromotedLHS, &ePromotedRHS))
		{
			assert(ePromotedLHS == ePromotedRHS);
			return LookupPrimitiveType(ePromotedLHS);
		}
		else
		{
			assert(false);
		}
	}
	else
	{
		// TODO
		assert(false);
	}

	return nullptr;
}

const SiType* BinaryOperation::GetType(BaseContext& context)
{
	switch (op) {
	case BinaryOp::Addition:
	case BinaryOp::Subtraction:
	case BinaryOp::Multiplication:
	case BinaryOp::Division:
	case BinaryOp::Modulus:
	case BinaryOp::BooleanAnd:
	case BinaryOp::BooleanOr:
	case BinaryOp::ShiftLeft:
	case BinaryOp::ShiftRight:
	case BinaryOp::BitwiseAnd:
	case BinaryOp::BitwiseOr:
	case BinaryOp::BitwiseXOr:
	{
		const SiType* pLHSType = lhs->GetType(context);
		const SiType* pRHSType = rhs->GetType(context);
		const SiType* pPromotedType = FindCompatibleTypes(op, pLHSType, pRHSType);
		return pPromotedType;
	}

	case BinaryOp::Equal:
	case BinaryOp::NotEqual:
	case BinaryOp::LessThan:
	case BinaryOp::LessThanOrEqual:
	case BinaryOp::GreaterThan:
	case BinaryOp::GreaterThanOrEqual:
	{
		const SiType* pBoolType = context.FindType("bool");
		return pBoolType;
	}

	default: return nullptr;
	}
}

llvm::Value* BinaryOperation::Evaluate(BaseContext& context) {

	llvm::Value* left = lhs->Evaluate(context);
	llvm::Value* right = rhs->Evaluate(context);

	int llvmOp = 0;

	switch (op) {
	case BinaryOp::Addition:           llvmOp = llvm::BinaryOperator::Add;           goto arithmetic; // TODO signed/float variants
	case BinaryOp::Subtraction:        llvmOp = llvm::BinaryOperator::Sub;           goto arithmetic; // TODO signed/float variants
	case BinaryOp::Multiplication:     llvmOp = llvm::BinaryOperator::Mul;           goto arithmetic; // TODO signed/float variants
	case BinaryOp::Division:           llvmOp = llvm::BinaryOperator::UDiv;          goto arithmetic; // TODO signed/float variants
	case BinaryOp::Modulus:            llvmOp = llvm::BinaryOperator::URem;          goto arithmetic; // TODO signed/float variants
	case BinaryOp::BooleanAnd:         return nullptr; // TODO
	case BinaryOp::BooleanOr:          return nullptr; // TODO
	case BinaryOp::ShiftLeft:          llvmOp = llvm::BinaryOperator::Shl;           goto arithmetic;
	case BinaryOp::ShiftRight:         llvmOp = llvm::BinaryOperator::LShr;          goto arithmetic;
	case BinaryOp::BitwiseAnd:         llvmOp = llvm::BinaryOperator::And;           goto arithmetic;
	case BinaryOp::BitwiseOr:          llvmOp = llvm::BinaryOperator::Or;            goto arithmetic;
	case BinaryOp::BitwiseXOr:         llvmOp = llvm::BinaryOperator::Xor;           goto arithmetic;
	case BinaryOp::Equal:              llvmOp = llvm::ICmpInst::Predicate::ICMP_EQ;  goto comparison;
	case BinaryOp::NotEqual:           llvmOp = llvm::ICmpInst::Predicate::ICMP_NE;  goto comparison;
	case BinaryOp::LessThan:           llvmOp = llvm::ICmpInst::Predicate::ICMP_ULT; goto comparison; // TODO signed/float variants
	case BinaryOp::LessThanOrEqual:    llvmOp = llvm::ICmpInst::Predicate::ICMP_ULE; goto comparison; // TODO signed/float variants
	case BinaryOp::GreaterThan:        llvmOp = llvm::ICmpInst::Predicate::ICMP_UGT; goto comparison; // TODO signed/float variants
	case BinaryOp::GreaterThanOrEqual: llvmOp = llvm::ICmpInst::Predicate::ICMP_UGE; goto comparison; // TODO signed/float variants
	case BinaryOp::Subscript:          return nullptr; // TODO create SiType with member elements
	default: return nullptr;
	}


arithmetic:
	return llvm::BinaryOperator::Create((llvm::BinaryOperator::BinaryOps)llvmOp, left, right, "", context.block);
	
comparison:
	return new llvm::ICmpInst(*context.block, (llvm::ICmpInst::Predicate)llvmOp, left, right);
}				   
				   
UnaryOperation::UnaryOperation(Expression* operand, UnaryOp op) {
	this->operand = operand;
	this->op = op;
}				   

const SiType* UnaryOperation::GetType(BaseContext& context)
{
	const SiType* pBaseType = operand->GetType(context);

	// TODO this is more complex than returning the exact same type
	switch (op) {
	case UnaryOp::Negation:   return pBaseType;
	case UnaryOp::BooleanNot: return pBaseType;
	case UnaryOp::BitwiseNot: return pBaseType;
	case UnaryOp::Invocation: return pBaseType;
	case UnaryOp::Index:      return pBaseType;
	}
}

llvm::Value* UnaryOperation::Evaluate(BaseContext& context) {
	llvm::Value* value = operand->Evaluate(context);
	
	// TODO do all of these need to be here, invocation and subscript specifically probably dont belong

	switch (op) {
	case UnaryOp::Negation:   return nullptr; // TODO
	case UnaryOp::BooleanNot: return nullptr; // TODO
	case UnaryOp::BitwiseNot: return nullptr; // TODO
	case UnaryOp::Invocation: return nullptr; // TODO
	case UnaryOp::Index:      return nullptr; // TODO
	}
}