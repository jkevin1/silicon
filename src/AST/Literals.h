#pragma once

#include "AST/ASTBase.h"
#include <stdint.h>
#include <string.h>

class SignedLiteral : public Expression {
public:
	SignedLiteral(int64_t value) : value(value) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	int64_t value;
};

class UnsignedLiteral : public Expression {
public:
	UnsignedLiteral(uint64_t value) : value(value) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	uint64_t value;
};

class FloatLiteral : public Expression {
public:
	FloatLiteral(double value) : value(value) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	double value;
};

class BoolLiteral : public Expression {
public:
	BoolLiteral(bool value) : value(value) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	bool value;
};

class StringLiteral : public Expression {
public:
	StringLiteral(const char* str) : value(str + 1), length(strlen(str) - 2) { }
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;
private:
	const char* value;
	unsigned length;
};