#include "SiliconType.h"
#include "llvm/IR/DerivedTypes.h"

//=============================================================================
// Built-in type IDs, the order matters 
//=============================================================================
enum SIType::TypeID : uint8_t {
	Void = 0,                 // void/unknown type
	Bool, Char,               // basic primitive types
	I8, I16, I32, I64, ISize, // signed integral types
	U8, U16, U32, U64, USize, // unsigned integral types
	F32, F64,                 // floating point types
	Array, String,            // array types
	Function,                 // function type
	Pointer,                  // pointer type
};

static const llvm::StringRef NameTable[] = {
	"void", "bool", "char",
	"i8", "i16", "i32", "i64", "isize",
	"u8", "u16", "u32", "u64", "usize",
	"f32", "f64",
};

bool SIType::IsBoolType() const {
	return (type == Bool);
}

bool SIType::IsCharType() const {
	return (type == Char);
}

bool SIType::IsNumericType() const {
	return (type >= I8 && type <= F64);
}

bool SIType::IsIntegralType() const {
	return (type >= I8 && type <= USize);
}

bool SIType::IsSignedIntegralType() const {
	return (type >= I8 && type <= ISize);
}

bool SIType::IsUnsignedIntegralType() const {
	return (type >= U8 && type <= USize);
}

bool SIType::IsFloatingPointType() const {
	return (type == F32 || type == F64);
}

bool SIType::IsPrimitive() const {
	return (type >= Bool && type <= F64);
}

size_t SIType::GetSize() const {
	switch (type) {
	// basic primitives
	case Bool:  return 1;
	case Char:  return 4; // UTF8
	// signed integers
	case I8:    return 1;
	case I16:   return 2;
	case I32:   return 4;
	case I64:   return 8;
	case ISize: return 8; // TODO
	// unsigned integers
	case U8:    return 1;
	case U16:   return 2;
	case U32:   return 4;
	case U64:   return 8;
	case USize: return 8; // TODO
	// floating point types
	case F32:   return 4;
	case F64:   return 8;
	// others are unknown, return 0, TODO error
	default:    return 0;
	}
}

size_t SIType::GetAlignment() const {
	return GetSize(); // TODO more specific
}

llvm::StringRef SIType::GetName() const {
	if (type <= F64) return NameTable[type];
	return llvm::StringRef("null"); // something bad happened, TODO error
}