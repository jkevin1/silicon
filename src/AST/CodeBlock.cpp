#include "CodeBlock.h"
#include "Compiler/Context.h"

void CodeBlock::Generate(BaseContext& context)
{
	for (auto& statement : statements->statements)
	{
		statement->Generate(context);
	}
}