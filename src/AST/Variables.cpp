#include "Variables.h"
#include "Compiler/Context.h"
#include "Silicon/SiType.h"
#include "llvm/IR/Instructions.h"

VariableDefinition::VariableDefinition(Identifier type, Identifier name, Expression* initializer) {
	this->type = type;
	this->name = name;
	this->initializer = initializer;
}

void VariableDefinition::Generate(BaseContext& context) {
	// allocate instance of type
	auto* varType = context.FindType(type);
	auto* variable = new llvm::AllocaInst(varType->GetLLVMType(), 0, name, context.block);
	context.DefineVariable(name, variable); // TODO
	if (initializer != nullptr)
		new llvm::StoreInst(initializer->Evaluate(context), variable, false, context.block);
}

const SiType* VariableReference::GetType(BaseContext& context)
{
	auto* variable = context.FindVariable(name);
	return variable->getType();
}

llvm::Value* VariableReference::Evaluate(BaseContext& context) {
	auto* variable = context.FindVariable(name);
	if (auto alloc = llvm::dyn_cast<llvm::AllocaInst>(variable))
		return new llvm::LoadInst(alloc, "", false, context.block);
	return variable;
}