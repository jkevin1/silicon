#pragma once

// TODO lighter weight StrRef?
#include "llvm/ADT/StringRef.h"

namespace llvm {
	class Type;
	class LLVMContext;
}

class SIType {
public:
	
	bool IsBoolType() const;
	bool IsCharType() const;
	bool IsNumericType() const;
	bool IsIntegralType() const;
	bool IsSignedIntegralType() const;
	bool IsUnsignedIntegralType() const;
	bool IsFloatingPointType() const;
	bool IsPrimitive() const;

	virtual size_t GetSize() const;
	virtual size_t GetAlignment() const;

	virtual llvm::StringRef GetName() const;

protected:
	enum TypeID : uint8_t;
	SIType(TypeID t) : type(t) { }
private:
	const TypeID type;
};

class SIArrayType : public SIType {
public:
	

	virtual size_t GetSize() const override;
	virtual size_t GetAlignment() const override;

	virtual llvm::StringRef GetName() const override;

private:
	enum AllocType : uint8_t;
	AllocType allocType;
	SIType* element;
};

class SIStringType : public SIArrayType {
public:

	bool IsUTF8();
	bool IsASCII(); // each character is one byte

	bool IsPackedUTF8(); // standard UTF8 encoding
	bool IsSparseUTF8(); // each codepoint is a full 4 bytes

	virtual size_t GetSize() const override;
	virtual size_t GetAlignment() const override;

	virtual llvm::StringRef GetName() const override;

private:

};