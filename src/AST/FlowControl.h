#pragma once

#include "AST/ASTBase.h"

class Branch : public Statement
{
public:
	Branch(Expression* condition, Statement* statement) : condition(condition), statement(statement), elseStatement(nullptr) { }
	void SetElse(Statement* statement) { elseStatement = statement; }
	virtual void Generate(BaseContext& context) override;
private:
	Expression* condition;
	Statement* statement;
	Statement* elseStatement;
};

class TernaryExpression : public Expression
{
public:
	TernaryExpression(Expression* cond, Expression* trueVal, Expression* falseVal);
	virtual const SiType* GetType(BaseContext& context) override;
	virtual llvm::Value* Evaluate(BaseContext& context) override;

private:
	Expression* cond;
	Expression* trueExpr;
	Expression* falseExpr;
};