%{
#include "Compiler/ModuleBuilder.h"
#define YYERROR_VERBOSE 1
%}

// these all need to be in the header
%code requires {
class ModuleBuilder;
class Expression;
class Statement;
class VariableDefinition;
class ArgumentList;
class StatementList;
class ExpressionList;
class CodeBlock;
class Branch;
typedef void* yyscan_t;
}

// these need to be in the implementation
// TODO just include generated flex header
%code {
union YYSTYPE;
extern int yylex(YYSTYPE * yylval_param, yyscan_t yyscanner);
void yyerror(ModuleBuilder*, yyscan_t, const char *s) { printf("Error: %s\n", s); }
}

// no global state, so multiple parsers can run at once
%define api.pure full

// don't generate line directives in the parser file
%no-lines

// the lexer's state to pass in
%lex-param { yyscan_t scanner }

// the parser's state to pass in
%parse-param { ModuleBuilder* module }
%parse-param { yyscan_t scanner }

// union of all possible node types
%union {
	const char* text;
	const char* string;
	Branch* branch;
    Statement* statement;
    Expression* expression;
	VariableDefinition* variabledefinition;
	StatementList* statementlist;
	CodeBlock* codeblock;
	ArgumentList* argumentlist;
	ExpressionList* expressionlist;
}

// this is an example grammar for C11
// http://www.quut.com/c/ANSI-C-grammar-y.html

// define terminal symbols, single chars are used directly
%token IDENTIFIER INTLITERAL FLTLITERAL STRLITERAL
%token LSHIFT RSHIFT LOGICAND LOGICOR
%token GREATEREQUAL LESSEQUAL EQUAL NOTEQUAL
%token PLUSASSIGN MINUSASSIGN TIMESASSIGN DIVIDEASSIGN
%token BOOL_TRUE BOOL_FALSE RETURN
%token IF ELSE FOR WHILE

%type <text> IDENTIFIER INTLITERAL FLTLITERAL STRLITERAL
%type <string> identifier
%type <expression> primary_expression
%type <expression> postfix_expression
%type <expression> unary_expression
%type <expression> cast_expression
%type <expression> multiplicative_expression
%type <expression> additive_expression
%type <expression> bitwise_expression
%type <expression> relational_expression
%type <expression> logical_expression
%type <expression> conditional_expression
%type <expression> assignment_expression
%type <expression> expression
%type <statement> expression_statement
%type <statement> statement
%type <statement> function_definition
%type <variabledefinition> variable_definition
%type <branch> if_statement
%type <branch> else_statement
%type <argumentlist> argument_list
%type <expressionlist> expression_list
%type <statementlist> statement_list
%type <codeblock> code_block

%start module
%%

// the lowest tier of expression
// other expressions use these as an input
identifier 
	: IDENTIFIER { $$ = module->CreateIdentifier($1); }

primary_expression
	: identifier { $$ = module->Create<VariableReference>($1); }
	| INTLITERAL { $$ = module->ParseInteger(); }
	| FLTLITERAL { $$ = module->ParseFloat(); }
	| STRLITERAL { $$ = module->ParseString(); }
	| BOOL_TRUE  { $$ = module->Create<BoolLiteral>(true); }
	| BOOL_FALSE { $$ = module->Create<BoolLiteral>(false); }
	| '(' expression ')' { $$ = $2; }
	;

// postfix expressions are processed early on
postfix_expression
	: primary_expression { $$ = $1; }
	| postfix_expression '[' expression_list ']' { printf("index operation\n"); }	// allow multiple arguments
	| postfix_expression '(' ')' { $$ = module->Create<FunctionCall>($1, nullptr); }
	| postfix_expression '(' expression_list ')' { $$ = module->Create<FunctionCall>($1, $3); }
	| postfix_expression '.' identifier { printf("subscript\n"); }
	// ++ or --?
	;

// a list of any type of expression
expression_list
	: expression { $$ = module->Create<ExpressionList>($1); }
	| expression_list ',' expression { $$ = $1; $$->AddExpression($3); }
	;

// unary expressions are processed next
unary_expression
	: postfix_expression { $$ = $1; }
	| '+' unary_expression { module->Log("positive"); }
	| '-' unary_expression { module->Log("negative"); }
	| '~' unary_expression { module->Log("bitwise not"); }
	| '!' unary_expression { module->Log("logical not"); }
	// TODO pointer stuff here?
	// ++ or --?
	;

// cast expressions are processed next	
// TODO c-style or not?
cast_expression
	: unary_expression { $$ = $1; }
	// this causes a shift reduce conflict, i think with '(' expression ')'
//	| '(' IDENTIFIER ')' cast_expression { printf("cast operation: %s\n", yyget_text(scanner)); }
	;
	
// additive expressions come next
multiplicative_expression
	: cast_expression { $$ = $1; }
	| multiplicative_expression '*' cast_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::Multiplication, $3); }
	| multiplicative_expression '/' cast_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::Division, $3); }
	| multiplicative_expression '%' cast_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::Modulus, $3); }
	;

// additive expressions come next
additive_expression
	: multiplicative_expression { $$ = $1; }
	| additive_expression '+' multiplicative_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::Addition, $3); }
	| additive_expression '-' multiplicative_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::Subtraction, $3); }
	;

// bitwise expressions come after math
// TODO what order should they be processed?
bitwise_expression
	: additive_expression { $$ = $1; }
	| bitwise_expression '&' additive_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::BitwiseAnd, $3); }
	| bitwise_expression '|' additive_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::BitwiseOr, $3); }
	| bitwise_expression '^' additive_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::BitwiseXOr, $3); }
	| bitwise_expression LSHIFT additive_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::ShiftLeft, $3); }
	| bitwise_expression RSHIFT additive_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::ShiftRight, $3); }
	;

// relational expressions are handled after all mathematics
relational_expression
	: bitwise_expression { $$ = $1; }
	| relational_expression '<' bitwise_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::LessThan, $3); }
	| relational_expression '>' bitwise_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::GreaterThan, $3); }
	| relational_expression GREATEREQUAL bitwise_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::GreaterThanOrEqual, $3); }
	| relational_expression LESSEQUAL bitwise_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::LessThanOrEqual, $3); }
	| relational_expression EQUAL bitwise_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::Equal, $3); }
	| relational_expression NOTEQUAL bitwise_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::NotEqual, $3); }
	;

// boolean logical expressions after that
logical_expression
	: relational_expression { $$ = $1; }
	| logical_expression LOGICAND relational_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::BooleanAnd, $3); }
	| logical_expression LOGICOR relational_expression { $$ = module->Create<BinaryOperation>($1, BinaryOp::BooleanOr, $3); }
	;

// ternary expression
conditional_expression
	: logical_expression { $$ = $1; }
	| logical_expression '?' expression ':' conditional_expression { $$ = module->Create<TernaryExpression>($1, $3, $5); }
	;
	
assignment_expression
	: postfix_expression '=' conditional_expression { printf("assignment operation\n"); }
	// TODO += -= etc
	;

// expression is just an alias of the highest level expression
expression
	: conditional_expression { $$ = $1; }
	| assignment_expression { $$ = $1; }
	;
	
code_block
	: '{' statement_list '}' { $$ = module->Create<CodeBlock>($2); }
	;

function_definition
	: identifier identifier '(' ')' code_block { $$ = module->Create<FunctionDefinition>($1, $2, nullptr, $5); }
	| identifier identifier '(' argument_list ')' code_block { $$ = module->Create<FunctionDefinition>($1, $2, $4, $6); }
	;

variable_definition
	: identifier identifier { $$ = module->Create<VariableDefinition>($1, $2, nullptr); }
	| identifier identifier '=' expression { $$ = module->Create<VariableDefinition>($1, $2, $4); }
	;
// TODO types and declarations

argument_list
	: variable_definition{ $$ = module->Create<ArgumentList>($1); }
	| argument_list variable_definition{ $$ = $1; $$->AddArgument($2); }
	;

expression_statement
	: expression ';' { $$ = module->Create<ExpressionStatement>($1); }
	| variable_definition ';' { $$ = $1; }
	;

if_statement
	: IF '(' expression ')' statement  { $$ = module->Create<Branch>($3, $5); }
	| IF '(' expression ')' code_block { $$ = module->Create<Branch>($3, $5); }
	;

else_statement
	: if_statement ELSE statement { $$ = $1; $$->SetElse($3); }
	| if_statement ELSE code_block{ $$ = $1; $$->SetElse($3); }
	;

statement
	: expression_statement { $$ = $1; }
	| function_definition { $$ = $1; }
	| if_statement { $$ = $1; }
	| else_statement { $$ = $1; }
	| RETURN expression ';' { $$ = module->Create<ReturnStatement>($2); }
	| RETURN ';' { $$ = module->Create<ReturnStatement>(); }
	// TODO the rest
	;

statement_list
	: statement { $$ = module->Create<StatementList>($1); }
	| statement_list statement{ $$ = $1; $$->AddStatement($2); }
	;

module
	: statement_list { module->SetModuleCode($1); }
	;

%%