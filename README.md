# README #

This is an experimental compiler for a high-level language deriving from C/C++, java, rust, and other languages.  

It can be built with cmake with LLVM and flex/bison installed.  On windows that's a bit of a hassle.  

To do list:  
-modularize into subprojects in cmake  
-wrap parser and lexer in a class such that multiple can exist at once  
-finish AST and grammar  
-cleanup